![nodejs](../../resources/nodejs.png)

---
# Sommaire

1. Qu'est-ce que Node.JS
2. Utilisations
3. Gestionnaire de paquet et package.json
4. EventLoop
5. Plan de travail

---
# Qu'est-ce que Node.JS

### Qu'est ce que c'est ??

- Javascript côté server
- Moteur V8 de Webkit
- Open Source
- Multiplateforme (ou presque)
- Mono thread
- Asynchrone
- I/O non bloqués

### Versions

| Release  | Status              | Codename    |Initial Release | Active LTS Start | Maintenance Start | End-of-life                |
| :--:     | :---:               | :---:       | :---:          | :---:            | :---:                 | :---:                     |
| [8.x][]  | **Maintenance LTS** | [Carbon][]  | 2017-05-30     | 2017-10-31       | 2019-01-01            | December 2019<sup>1</sup> |
| [10.x][] | **Active LTS**      | [Dubnium][] | 2018-04-24     | 2018-10-30       | April 2020            | April 2021                |
| [12.x][] | **Current Release** |             | 2019-04-23     | 2019-10-21       | October 2021          | April 2022                |
| 13.x     | **Pending**         |             | 2019-10-22     |                  |                       | June 2020                 |
| 14.x     | **Pending**         |             | April 2020     | October 2020     | October 2022          | April 2023                |

[source](https://github.com/nodejs/Release)


- Deux types de version :
	- LTS (nombres paires): Support éttendu : Ce sont les versions les plus stables
	- Les autres (nombres impaires): versions dites "beta" (test de nouvelles fonctionnalités, ...)


### Server HTTP

- serveur http

```javascript
var http = require('http');

http.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end('Hello World!');
}).listen(8080);
```

---
# Utilisations

- scripts bash
- compilations diverses (babel, ...)
- applications multiplateformes desktop (electron, NW)
- applications mobiles (react-native, ionic, native-script)
- serveurs web (express, hapi, koa)
- serverless (chromeless, ...)
- ...

---
# Gestionnaire de paquets


- NPM
	- officiel
	- npmjs.org

```
npm install -S hapi
```

- Yarn
	- facebook

```
yarn add hapi
```

### NPX

- Permet de lancer des binaires installés via npm
- Peu importe s'ils sont au sein du projet ou global

```
./node_modules/.bin/eslint
/usr/bin/eslint

npx eslint
```

### Package.json

- coeur de l'application
- paquets utilisés
- commandes de lancement
- version Node.JS requise

---

```
{
  "private": true,
  "name": "Test APP",
  "description": "This is an example package.json",
  "version": "0.0.0-this-does-not-matter",
  "license": "MIT",
  "scripts": {
    "dev": "npx poi",
  },
  "devDependencies": {
    "less": "2.7.2",
    "less-loader": "4.0.5",
  },
  "dependencies": {
    "lodash": "4.17.4",
    "moment": "2.18.1",
  }
}
```

- **scripts** : scripts à lancer via `npm run`
- **devDependencies** : Dépendances servant uniquement pour la phase de développement
- **dependencies** : dépendances du projet
- **private** : Si à `true`, le projet ne sera jamais publié sur npm si vous lancez `npm publish` par mégarde

---
# EventLoop


```
   ┌───────────────────────┐
┌─>│        timers         │
│  └──────────┬────────────┘
│  ┌──────────┴────────────┐
│  │     I/O callbacks     │
│  └──────────┬────────────┘
│  ┌──────────┴────────────┐
│  │     idle, prepare     │
│  └──────────┬────────────┘      ┌───────────────┐
│  ┌──────────┴────────────┐      │   incoming:   │
│  │         poll          │<─────┤  connections, │
│  └──────────┬────────────┘      │   data, etc.  │
│  ┌──────────┴────────────┐      └───────────────┘
│  │        check          │
│  └──────────┬────────────┘
│  ┌──────────┴────────────┐
└──┤    close callbacks    │
   └───────────────────────┘
```

### Détails

- **timers** : lance les callbacks programmés par `setTimeout` et `setIntervale`
- **I/O callbacks** : lance tous les autres callbacks
- **idle, prepare** : utilisé par le coeur de Node.JS
- **poll** : Créer de nouveaux événements d'entrée/sortie. Le cas échéant, peut bloquer l'exécution du script.
- **check** : invoque les callbacks de `setImmediate`
 - **close callbacks** : `socket.on('close')`, ...

- Permet l'asynchrone
- Possibilité de forcer l'exécution au prochain passage avec `process.nextTick()`
- Fonctions avec callback pas toujours asynchrones selon ce qu'elles font
- Promesses toujours Asynchrones


### Examples

#### Fonction synchrone
```javascript
const funcSync = callback => {

    const fields = {
        foo : 'foo'
    };
    callback(fields);
};

console.log('a');
funcSync(field => console.log); // funcSync(field => console.log(field));
console.log('fin');
```

```
a
{foo: "foo"}
fin
```


#### nextTick

```javascript
const funcSync = callback => {

    const fields = { foo : 'foo'};
    process.nextTick(() => {

    	callback(fields);
    });
};

console.log('a');
funcSync(field => console.log);
console.log('fin');
```

```
a
fin
{foo: "foo"}
```

### Exceptions

- **Attention** : tous les callbacks ne sont pas asynchrones de base :

```javascript
const a = [1, 2, 3, 4];

a.forEach((value) => {

    console.log(value);
});

console.log('after forEach');
```
- Les fonctions des types de base (String, Object, Array, ...) sont toujours synchrones.

---
# Les événements

- Utilisés au travers de la classe `EventEmitter`
	```javascript
    const EventEmitter = require('events').EventEmitter;
    const myEventNamespace = new EventEmitter();
    ```
- Callbacks effectués à des moments précis
- Appels via `event.emit('eventName', ...params)`
- Interceptés via `event.on('eventName', callbackFunc)`

### .then & .catch

```javascript
 myPromiseFunction()
    .then((val) => console.log(val))
    .catch(TypeError, err => console.error)
    .catch(ReferenceError, err => console.error)
    .catch(err => console.error('Error non gérée', err));
```

### async & await

```javascript

try {

  const val = await myPromiseFunction();
  console.log(val);
}
catch(error){

  if(error instanceof TypeError){
    console.error('Type Error');
  }

  if(error instanceof ReferenceError){
    console.error('Reference error');
  }

  console.error(error);
}

```

- Asynchrones
- Capturent les erreurs
- Transformation fonction callback en promesses via `util.promisify`

```javascript
const Util = require('util');
const Fs   = require('fs');

const ReadFileAsync = Util.promisify(Fs.readFile);

(async () => {

  try {
    const text = await ReadFileAsync(filePath, { encoding: 'utf8' });
    console.log(text);
  }
  catch(error){

    console.error(error);
  }
})();

```

- Norme A+ : Utilisation de libs compatibles
- Manipulation des Arrays en asynchrone :
	- `Promise.map`
	- `Promise.each`
- Fonctions étendues compatibles A+ au sein de la library `bluebird`

---
# Mise en production

- Serveurs Web :
	- PM2 : Permet de lancer le serveur comme un service et de le relancer automatiquement
	- Docker : via un système d'orchestration (Kubernetes, ...)
- Module :
	- NPM et Yarn
- Scripts :
	- Services functions (AWS lambda, ...)    

---
# Plan de travail : Outils

- Utilisation de NVM avec la dernière version LTS de Node.JS en date (12.X)
- Utilisation de Webstorm (licences gratuites étudiants)
- Repository Git (gitlab ou github) pour le suivi du projet (doit etre accesible publiquement)

---
# Organisation

- TD : Introduction
- TP1 : Analyse, Debug et Eslint
- TP2 : NPM (création d'un module NPM)
- TP3 : Promesses
- TP4 : Hapi (bases / hapipal)
- TP5 : Hapi (joi / swagger)
- TP5 : Hapi (objection / migrations )
- TP6 : Hapi (project)

---
# Objectifs

- Comprendre Node.JS
- Utiliser Hapi.JS
- Faire une API REST
- Interragir avec une base de données
- Approche microService
