# Authentification

Dans le dernier TP vous avez créer des routes afin d'intéragir avec votre base de données de films. Par contre il vous manque surement la partie authentification. Vous devez faire en sorte que votre `token` obtenu a partir de votre service utilisateur fonctionne comme authentification sur le service filme que vous venez de créer.


# Service paiement

C'est dernier service que vous aurez a créer. L'objectif est de rélier les 3 services. Vous avez des utilisateurs, vous les authentifiez sur vos services et vos avez une base de données de filmes. Le service paiement devra permettre (de façon fictif) a un utilisateur d'ajouter / supprimer des films d'un panier et pour finir d'acheter ces films.  Vous devez donc mettre en place les routes nécessaires au bon fonctionnement de l'API et modifier le service film afin de pouvoir associer un prix a vos films en base de données (pensez au migrations). On suppose que les films sont en version dématérialisé.

 - Lorsqu'un utilsateur ajoute des élements a votre panier vous devrez vous assurez que le film existe bien sur le service film.
 - Lorsqu'il veut récupérer son panier, il vous faudrait lister les articles avec leur prix a jour, et le calcul du TVA
 - Il faudra finalement une route permetant d'acheter le panier, avec un moyen de payement (fictif).


Pour la communication entre service vous pouvez utiliser une librairie du style `got`. N'oubliez pas que vous devrez passer votre token sur les appels entre service. C'est une bonne pratique des créer un classe par API qui s'occupera des faires les appels.

### Suite

 - Faire en sorte de vérifié et mettre a jour le panier lorsqu'un film a été supprimé et qu'on souhaite récuperer le panier ou qu'on souhaite payer.
 - Pouvoir gérer plusieurs paniers
 - (BONUS) Rajouter des utilisateur administrateurs (capable de gérer les prix, l'ajout de films etc) (https://hapi.dev/api/?v=19.0.5#-routeoptionsauthaccessscope) 
 - (BONUS) Mettre la possiblité de faire des code promos (par un administrateur)  



### Notation

C'est l'ensemble des TP qui seront a rendre, avec un explicatif de comment les lancer et configurer pour qu'ils fonctionnent entre eux. La majorité de la notation portera sur le service paiement.


