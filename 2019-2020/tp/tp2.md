# User Service

### Objectifs

    - Créer un service utilisateur ensemble
    - Comprendre hapi & hapipal
    - Créer une base de données
    - Créer une route
    - Authentification

### Création du projet


Nous allons nous servir de hapipal. Pour créer un projet hapi en utilisant hapipal il faut exécuter la commande suivante:

```
npx hpal new user-service
```

Ensuite nous allons "commit" notre projet:

```
cd user-server

git init

git add --all

git commit -m "Initial commit"
```

Nous allons maintenant ajouter "swagger" a notre projet. C'est une interface qui est generé automatiquement pour notre API.

```
git cherry-pick swagger

git add .

git commit -m "swagger"
```

Nous allons aussi utiliser "objection.js" un ORM pour nodejs utilisant "knex".

```
git cherry-pick objection

git add . 


git commit -m "objection"
```

Il faudra modifier le  manifest pour configurer les elements que nous voulons utiliser sur notre projet

Modification de la partie schwifty `server/manifest.js`
```js
 {
                plugin  : 'schwifty',
                options : {
                    $filter    : 'NODE_ENV',
                    $default   : {},
                    $base      : {
                        migrateOnStart : true,
                        knex           : {
                            client           : 'pg',
                            connection       : {
                                host     : process.env.DB_HOST || '127.0.0.1',
                                user     : process.env.DB_USER || 'hapi',
                                password : process.env.DB_PASSWORD || 'hapi',
                                database : process.env.DB_DATABASE || 'user'
                            }
                        }
                    },
                    production : {
                        migrateOnStart : false
                    }
                }
}
```

package.json

```
"{
  "name": "iut-user-service",
  "version": "1.0.0",
  "description": "",
  "author": "",
  "license": "ISC",
  "main": "lib/index.js",
  "directories": {
    "lib": "lib",
    "test": "test"
  },
  "scripts": {
    "start": "node server",
    "test": "lab -a @hapi/code -L",
    "lint": "eslint ."
  },
  "dependencies": {
    "@hapi/boom": "8.x.x",
    "@hapi/hapi": "18.x.x",
    "@hapi/joi": "16.x.x",
    "bcrypt": "^3.0.6",
    "blipp": "^4.0.1",
    "hapi-auth-jwt2": "^8.8.0",
    "hapi-swagger": "11.0.0",
    "haute-couture": "3.x.x",
    "jsonwebtoken": "^8.5.1",
    "objection": "1.x.x",
    "pg": "7.12.1",
    "schmervice": "1.3.0",
    "schwifty": "4.x.x"
  },
  "peerDependencies": {
    "knex": "0.19.x"
  },
  "devDependencies": {
    "confidence": "4.x.x",
    "@hapi/code": "6.x.x",
    "@hapi/eslint-plugin-hapi": "4.x.x",
    "@hapi/glue": "6.x.x",
    "@hapi/inert": "5.x.x",
    "@hapi/hoek": "8.x.x",
    "@hapi/lab": "20.x.x",
    "@hapi/eslint-config-hapi": "12.x.x",
    "@hapi/vision": "5.x.x",
    "dotenv": "8.x.x",
    "eslint": "6.x.x",
    "hpal": "2.x.x",
    "hpal-debug": "1.x.x",
    "knex": "0.19.x",
    "sqlite3": "4.x.x",
    "toys": "2.x.x"
  }
}
  ```

installer les packages
```
npm i
```


Installer le pilot postgres:

```
npm i --save pg
```

Puis démarer le serveur:

```
npm start
```

Vous pouvez acceder a l'interface swagger a l'addresse suivant: http://localhost:3000/documentation. Vous remarquerez qu'il n'y pas de route, nous allons voir par la suite comment les créer.



### Migrations

Les "migrations" définissent par le code, les changement a effectué sur la base de données. Objection s'occuper de s'assurer que les migrations sont appliqués dans l'ordre et qu'une seule fois. Pour créer notre premiere migration (l'initialisation de la base de données) il faudra lancer la commande suivante

```
npx knex migrate:make user
```

Puis dans le fichier créer dans lib/migrations/xxx.js, nous allons definir la table 'user'. La fonction up(knex) définit ce qui sera lancé lorsque la migration est appliqué, la fonction down(knex) définit ce qu'il sera lancé lorsque la migration sera "rollback" (si on souhait l'annuler). Le down devrait donc permettre de remettre la base de données à l'état précedent. 

```js
'use strict';

module.exports = {
    async up(knex) {

        await knex.schema.createTable('user', (table) => {

            table.increments('id');
            table.string('username').unique();
            table.string('email').unique();
            table.string('firstName');
            table.string('lastName');
            table.string('password');

            table.dateTime('createdAt').notNull().defaultTo(knex.fn.now());
            table.dateTime('updatedAt').notNull().defaultTo(knex.fn.now());
        });
    },
    async down(knex) {

        await knex.schema.dropTableIfExists('user');
    }
};
```

Pour intéragir avec la table que nous venons de définir, il faudra créer un "model". Nous pourrons plus tard avoir accès a ce model avec `server.models();`. Pour créer le model:

```
npx hpal make model user
```

un fichier sera créer dans `lib/models`. Le module `schwifty` permet de rajouter de la validation a nos model de données en utilisant `@hapi/joi` (https://hapi.dev/family/joi/). (Joi est un puisant validateur en js, il nous permettra de valider toutes les données des utilisateurs de l'application). Sur le model on doit donc définit la table de la base de données a utiliser, le "schema" de validation joi, et des actions a effectuer avec un insert (`$beforeInsert`) et avant un update (`$beforeUpdate`). Il y a d'autres méthodes qu'on peut surcharger sur la classe, il suffit de regarder sur la documentation d'objection js (https://vincit.github.io/objection.js/)

```js
'use strict';

const Schwifty = require('schwifty');
const Joi      = require('@hapi/joi');

module.exports = class User extends Schwifty.Model {

    static get tableName() {

        return 'user';
    }

     static field(name) {

        return this.getJoiSchema().extract(name)
            .optional()
            .options({ noDefaults : true });
    }

    static get joiSchema() {

        return Joi.object({
            id        : Joi.number().integer(),
            username  : Joi.string().max(255),
            email     : Joi.string().email(),
            firstName : Joi.string().max(100),
            lastName  : Joi.string().max(100),
            password  : Joi.string(),
            createdAt : Joi.date(),
            updatedAt : Joi.date()
        });
    }

    $beforeInsert() {

        this.updatedAt = new Date();
        this.createdAt = this.updatedAt;
    }

    $beforeUpdate() {

        this.updatedAt = new Date();
    }

};
```

### Routes

C'est bien d'avoir un model et une base de données, mais il faudrait pouvoir intéragir avec. Les routes vont nous le permettre. Pour créer une route rien de plus simple que:

```
npx hpal make routes user
```

Une route a plusieurs elements important:

- method: la methode http qu'il faudra utiliser pour acceder a la route
- path: le chemin d'access, qui peut contenir des variables
- handler: Une fonction qui sera exécuté lorsque la route est appelé
- options.validation: La valiation des données en parametres a la route 
    - payload: le body de la requete http
    - params:  les parties variable de l'url 
    - query: tout ce qui est query string (`?id=1`)

Il y a plein d'options disponible sur hapi (https://hapi.dev/api/?v=18.4.0#route-options)


On utilise aussi `toys` qui avec la méthode `withRouteDefaults` nous permet d'appliquer la même configuration a toutes nos routes.

Nous allons donc créer les routes CRUD classique, nous intéragissons avec des "services" dans nos handlers, nous verons a la suite, a quoi ils servent et comment les déclarer.

```js
'use strict';

const Toys = require('toys');
const Joi  = require('@hapi/joi');
const User = require('../models/user');

const defaults = Toys.withRouteDefaults({
    options : {
        tags     : ['api', 'user'],
        response : {
            schema : Joi.object({
                id        : User.field('id'),
                username  : User.field('username'),
                firstName : User.field('firstName'),
                lastName  : User.field('lastName'),
                email     : User.field('email'),
                createdAt : User.field('createdAt'),
                updatedAt : User.field('updatedAt')
            })
        }
    }
});

module.exports = defaults([
    //TODO: Créer la route GET /users pour lister les utilisateurs
    //TODO: Créer la route GET /user/{id} pour récuperer un utilisateur
    //TODO: Créer la route PATCH /user/{id} pour modifier un utilisateur
    {
        method  : 'post',
        path    : '/user',
        options : {
            validate : {
                payload : Joi.object({
                    username  : User.field('username').required(),
                    firstName : User.field('firstName').required(),
                    lastName  : User.field('lastName').required(),
                    email     : User.field('email').required(),
                    password  : User.field('password').required()
                })
            }
        },
        handler : (request) => {

            const { userService } = request.services();

            return userService.create(request.payload);
        }
    },
    {
        method  : 'delete',
        path    : '/user/{id}',
        options : {
            validate : {
                params : Joi.object({
                    id : User.field('id')
                })
            },
            response : {
                emptyStatusCode : 204
            }
        },
        handler : async (request) => {

            const { userService } = request.services();

            return userService.delete(request.params.id);
        }
    }
]);
```

Il vous faudra compléter par trois routes suplémentaires, un pour lister, un pour récuperer un utilisateur par sont identifiant, et puis un pour modifier un utilisateur


### Services

Un service est une class réutilisable au sein du plugin. On peut récuperer l'instance du service a partir de `server` ou `request`. C'est ici qu'on essayera de mettre la plus part de notre code. Pour créer un service:

```
npx hpal make services user
```



```js
'use strict';

const Schmervice = require('schmervice');

const internals = {
    removePassword : (user) => {

        delete user.password;

        return user;
    }
};

module.exports = class UserService extends Schmervice.Service {

    list(ctx = {}) {

        // TODO: Completer le code ici
    }

    findById(id, ctx = {}) {

       // TODO: Completer le code ici
    }

    create(user, ctx = {}) {

        const { User } = this.server.models();

        return User.query(ctx.trx).insert(user).traverse(internals.removePassword);
    }

    update(id, user, ctx = {}) {

        // TODO: Completer le code ici
    }

    delete(id, ctx = {}) {

        const { User } = this.server.models();

        return User.query(ctx.trx).delete().where({ id });
    }
};
```

Il vous faudra compléter le service pour que vous routes ajoutés précedament fonctionnent.


### Authentication

Nous allons devoir authentifier l'utilisateur, pour ce faire nous allons utiliser un JWT (json web token). L'utilisateur devra envoyer son "username" et "password" et si les valeurs sont bonnes, un token lui sera retourné. Qu'il devra joindre a ces prochaines requetes dans le Header `Authorization` sous le format suivant:
`Authorization Bearer tokenici`


Commencer par créer un autre service, appeler `auth` qui contiendera a minima une méthode `login` prenant les informations de connexion d'un utilisateur en parametre et retournera soit un token valid si le mot de passe et nom d'utilisateur correspondent, soit une erreur Http 403 Forbiden (voir le package `@hapi/boom` pour la génération des erreurs). Vous aurez besoin des librairies suivant: 

`@hapi/boom`, `bcrypt`, `jsonwebtoken`

contenu du fichier `lib/services/auth.js` de base

```js
'use strict';

const Schmervice = require('schmervice');
const Boom       = require('@hapi/boom');
const Bcrypt     = require('bcrypt');
const Jwt        = require('jsonwebtoken');

module.exports = class AuthService extends Schmervice.Service {

    async login(username, password, ctx = {}) {

    
    }
};
```

Pour qu'on puisse appliquer a nos route l'authentificiation il faut créer une strategy d'authentification

```
npx hpal make auth/strategies jwt 
```

on utilse le plugin `hapi-auth-jwt2` il faudra donc l'installer aussi 

```
npm i --save hapi-auth-jwt2
```

et l'ajouter a notre manifest

```js
{
    plugin : 'hapi-auth-jwt2'
}
```

puis dans le fichier `lib/auth/strategies/jwt.js:

```js
'use strict';

module.exports = (server, options) => {

    return {
        name    : 'jwt',
        scheme  : 'jwt',
        options : {
            key      : options.jwtToken,
            validate : async (decoded, request) => {

                const { userService } = request.services();

                try {

                    await userService.findById(decoded.id);

                    return { isValid : true };
                } catch (error) {

                    return { isValid : false };
                }
            }
        }
    };
};
```

mettre la strategy par default

```
npx hpal make auth/default 
```

le fichier `lib/auth/default.js`

```js
'use strict';

module.exports = 'jwt';
```


Vous devrez completer la methode `login` et faire le nécesaire pour que les mot de passes des utilisateurs nouvellement créer soit crypté (pensez a utiliser `bcrypt`). Pensez a utiliser le service auth pour crypter et decrypter les mot de passe, afin de permettre un changement futur du method de cryptage si besoin. N'oubliez pas de désactiver l'authentification sur la route création d'utilisateur et de créer une route permettant l'utilisateur de se connecter et récuperer sont `JWT` (`POST` `/user/login` avec `username` et `password` en `payload`).

Il faut ajouter au options du plugin hapi-swagger

```
 securityDefinitions : {
           'jwt' : {
                'type' : 'apiKey',
                 'name' : 'Authorization',
                  'in'   : 'header'
        }
  ,
  security            : [{ 'jwt' : [] }]
```


