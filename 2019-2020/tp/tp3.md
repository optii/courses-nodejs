# Film service

Le but de ce TP est de mettre en place le service film. Pour ce faire, inspirez vous des TP précedents pour créer un nouveau projet hapi a part. Ce service devra permettre de rechercher, lister, modifier, supprimer, voir, et ajouter des films. Afin d'avoir des données, vous allez pouvoir utiliser votre module créer au TP précedent afin d'importer des films.



## Routes

Il faudra créer plusieurs route qui devront faire ce qui suit et retourner du JSON valide en fonction:


### Liste :

URL: `/films`     
Method: `GET`   
Détail:  Retourner la liste des films présents en base de données.Il faudra pouvoir chercher dans les films a l'aide de `query` paramètres. Par exemple `/films?name:like=Inceptio`


### Get : 

URL: `/film/{id}`   
Method: `GET`  
Detail: Retourne les détails d'un film spécifique.  

### Create

URL: `/film`  
Method: `POST`  
Detail: Permet de créer un film. Retourne le nouveau film créé  

### Update:

URL: `/film/{id}` 
Method: `PATCH`  
Detail: Permet de modifier un film spécifique. Retourne le film après modification  

### Delete

URL: `/film/{id}`
Method: `DELETE`
Detail: Permet de supprimer un film spécique. Doit retourner un resultat vide avec un code 204


